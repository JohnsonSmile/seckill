// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/2 21:13
// 文件名称 ：   order.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package datamodels

type Order struct {
	ID          int64 `json:"id" sql:"ID"`
	UserId      int64 `json:"userID" sql:"userID"`
	ProductId   int64 `json:"productID" sql:"productID"`
	OrderStatus int64 `json:"orderStatus" sql:"orderStatus"`
}

const (
	OrderWait = iota
	OrderSuccess
	OrderFailed
)
