// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/2 14:59
// 文件名称 ：   product.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package datamodels

type Product struct {
	ID           int64  `json:"id" sql:"ID"  seckill:"id"`
	ProductName  string `json:"ProductName" sql:"productName" seckill:"ProductName"`
	ProductNum   string `json:"ProductNum" sql:"productNum" seckill:"ProductNum"`
	ProductImage string `json:"ProductImage" sql:"productImage" seckill:"ProductImage"`
	ProductUrl   string `json:"ProductUrl" sql:"productUrl" seckill:"ProductUrl"`
}
