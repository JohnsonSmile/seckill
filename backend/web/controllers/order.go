// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/2 22:00
// 文件名称 ：   order.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package controllers

import (
	"backend/services"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
)

type OrderController struct {
	Ctx          iris.Context
	OrderService services.IOrderService
}

func (o *OrderController) Get() mvc.View {
	orderInfo, err := o.OrderService.GetAllOrderInfo()
	if err != nil {
		o.Ctx.Application().Logger().Debug("查询订单信息失败!")
	}
	return mvc.View{
		Name: "order/view.html",
		Data: iris.Map{
			"order": orderInfo,
		},
	}
}
