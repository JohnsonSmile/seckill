// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/2 21:47
// 文件名称 ：   order.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package services

import (
	"backend/datamodels"
	"backend/repositories"
)

type IOrderService interface {
	GetOrderByID(id int64) (order *datamodels.Order, err error)
	DeleteOrderByID(id int64) bool
	UpdateOrder(order *datamodels.Order) (err error)
	InsertOrder(order *datamodels.Order) (id int64, err error)
	GetAllOrder() (orders []*datamodels.Order, err error)
	GetAllOrderInfo() (orderInfo map[int]map[string]string, err error)
}

type OrderService struct {
	orderRepository repositories.IOrderRepository
}

func NewOrderService(repository repositories.IOrderRepository) IOrderService {
	return &OrderService{orderRepository: repository}
}

func (os *OrderService) GetOrderByID(id int64) (order *datamodels.Order, err error) {
	return os.orderRepository.SelectByKey(id)
}

func (os *OrderService) DeleteOrderByID(id int64) bool {
	return os.orderRepository.Delete(id)
}

func (os *OrderService) UpdateOrder(order *datamodels.Order) (err error) {
	return os.orderRepository.Update(order)
}

func (os *OrderService) InsertOrder(order *datamodels.Order) (id int64, err error) {
	return os.orderRepository.Insert(order)
}

func (os *OrderService) GetAllOrder() (orders []*datamodels.Order, err error) {
	return os.orderRepository.SelectAll()
}

func (os *OrderService) GetAllOrderInfo() (orderInfo map[int]map[string]string, err error) {
	return os.orderRepository.SelectAllWithInfo()
}
