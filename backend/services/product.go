// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/2 16:27
// 文件名称 ：   product.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package services

import (
	"backend/datamodels"
	"backend/repositories"
)

type IProductService interface {
	GetProductById(id int64) (p *datamodels.Product, err error)
	GetAllProduct() (ps []*datamodels.Product, err error)
	DeleteProductById(id int64) (suc bool)
	InsertProduct(p *datamodels.Product) (id int64, err error)
	UpdateProduct(p *datamodels.Product) (err error)
}

type ProductService struct {
	productRepository repositories.IProduct
}

func NewProductService(productRepository repositories.IProduct) IProductService {
	return &ProductService{productRepository: productRepository}
}

func (ps *ProductService) GetProductById(id int64) (p *datamodels.Product, err error) {
	return ps.productRepository.SelectByKey(id)
}

func (ps *ProductService) GetAllProduct() (pl []*datamodels.Product, err error) {
	return ps.productRepository.SelectAll()
}

func (ps *ProductService) DeleteProductById(id int64) (suc bool) {
	return ps.productRepository.Delete(id)
}

func (ps *ProductService) InsertProduct(p *datamodels.Product) (id int64, err error) {
	return ps.productRepository.Insert(p)
}

func (ps *ProductService) UpdateProduct(p *datamodels.Product) (err error) {
	return ps.productRepository.Update(p)
}
