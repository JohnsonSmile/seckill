// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/2 12:44
// 文件名称 ：   main.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package main

import (
	"backend/common"
	"backend/repositories"
	"backend/services"
	"backend/web/controllers"
	ctext "context"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/context"
	"github.com/kataras/iris/v12/mvc"
	"log"
)

func main() {
	// 1. 创建iris实例
	app := iris.New()
	// 2. 设置错误日志模式
	app.Logger().SetLevel("debug")
	// 3. 注册模板
	template := iris.HTML("./web/views", ".html").
		Layout("shared/layout.html").
		Reload(true)
	app.RegisterView(template)
	// 4. 设置静态文件目录
	//app.Favicon("./resources/favicon.ico")
	app.HandleDir("/assets", "./web/assets")
	app.OnAnyErrorCode(func(c context.Context) {
		c.ViewData("message", c.Values().GetStringDefault("message", "访问的页面出错"))
		c.ViewLayout("")
		c.View("shared/error.html")
	})

	// 连接数据库
	db, err := common.NewMysqlConn()
	if err != nil {
		log.Println(err)
	}

	// 5. 注册控制器
	ctx, cancel := ctext.WithCancel(ctext.Background())
	defer cancel()
	productManager := repositories.NewProductManager("product", db)
	productService := services.NewProductService(productManager)
	productParty := app.Party("/product")
	product := mvc.New(productParty)
	product.Register(ctx, productService)
	product.Handle(new(controllers.ProductController))

	orderManager := repositories.NewOrderManager("order", db)
	orderService := services.NewOrderService(orderManager)
	orderParty := app.Party("/order")
	order := mvc.New(orderParty)
	order.Register(ctx, orderService)
	order.Handle(new(controllers.OrderController))

	// 6. 启动服务
	app.Run(
		iris.Addr(":8080"),
		iris.WithoutServerError(iris.ErrServerClosed),
	)
}
