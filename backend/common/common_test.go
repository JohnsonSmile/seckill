// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/2 16:19
// 文件名称 ：   common_test.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package common

import (
    "backend/datamodels"
    "testing"
)

func TestDataToStructByTagSql(t *testing.T) {
	m := map[string]string{
		"ID":           "1",
		"productName":  "苹果",
		"productNum":   "1000",
		"productImage": "http://image",
		"productUrl":   "http://url",
	}
	p := &datamodels.Product{}
	DataToStructByTagSql(m, p)
	t.Log(p)
}
