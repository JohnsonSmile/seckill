// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/1 21:05
// 文件名称 ：   consume1.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package main

import (
	"cn.johnsonsmile.seckill/rabbitmq"
	"strconv"
	"time"
)

func main() {
	mq := rabbitmq.NewRabbitMQPubSub("PublishExchange")
	for i := 0; i < 100; i++ {
		mq.PublishPub("第" + strconv.Itoa(i) + "你好!")
		time.Sleep(time.Second)
	}
}
