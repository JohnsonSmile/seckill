// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/1 21:05
// 文件名称 ：   consume1.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package main

import "cn.johnsonsmile.seckill/rabbitmq"

func main() {
    mq := rabbitmq.NewRabbitMQPubSub("PublishExchange")
    mq.ReceiveSub()
}
