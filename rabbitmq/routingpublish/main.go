// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/1 21:43
// 文件名称 ：   main.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package main

import (
	"cn.johnsonsmile.seckill/rabbitmq"
	"strconv"
)

func main() {
	mq1 := rabbitmq.NewRabbitMQRouting("RoutingExchange", "route1")
	mq2 := rabbitmq.NewRabbitMQRouting("RoutingExchange", "route2")
	for i := 0; i < 100; i++ {
		if i%2 == 0 {
			mq1.PublishRouting("Hello: " + strconv.Itoa(i))
		} else {
			mq2.PublishRouting("Hello: " + strconv.Itoa(i))
		}
	}
}
