// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/2 00:01
// 文件名称 ：   movie.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package services

import (
	"app/repositories"
	"fmt"
)

type MovieService interface {
	ShowMovieName() string
}

type MovieServiceManager struct {
	repo repositories.MovieRepository
}

func (m *MovieServiceManager) ShowMovieName() string {
	fmt.Println("获取的视频名称为: " + m.repo.GetMovieName())
	return "获取的视频名称为: " + m.repo.GetMovieName()
}

func NewMovieServiceManager(repo repositories.MovieRepository) MovieService {
	return &MovieServiceManager{repo: repo}
}
