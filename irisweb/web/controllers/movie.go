// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/2 00:04
// 文件名称 ：   movie.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package controllers

import (
    "app/repositories"
    "app/services"
    "github.com/kataras/iris/v12/mvc"
)

type MovieController struct {

}

func (m *MovieController) Get() mvc.View {
    movieRepository := repositories.NewMovieManager()
    movieService := services.NewMovieServiceManager(movieRepository)
    movieResult := movieService.ShowMovieName()
    return mvc.View{
        Name:   "movie/index.html",
        Data:   movieResult,
    }
}
