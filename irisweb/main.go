// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/1 23:49
// 文件名称 ：   main.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package main

import (
	"app/web/controllers"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
)

func main() {
	app := iris.New()
	app.Logger().SetLevel("debug")
	app.RegisterView(iris.HTML("./web/views", ".html"))

	// 注册控制器
	mvc.New(app.Party("/hello")).Handle(new(controllers.MovieController))
	err := app.Run(iris.Addr(":8080"))
	if err != nil {
		app.Logger().Println(err)
	}

}
