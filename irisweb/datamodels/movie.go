// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/1 23:57
// 文件名称 ：   movie.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package datamodels

type Movie struct {
    Name string
}
