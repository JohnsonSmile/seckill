// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/3 13:34
// 文件名称 ：   product.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package controllers

import (
	"foreground/datamodels"
	"foreground/services"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"html/template"
	"os"
	"path/filepath"
	"strconv"
)

type ProductController struct {
	Ctx            iris.Context
	ProductService services.IProductService
	OrderService   services.IOrderService
}

// 生成静态页面
var (
	htmlOutPath  = "./web/htmlProductShow/" // 生成的html保存的目录
	templatePath = "./web/views/template/"
)

// 静态文件的路由
func (p *ProductController) GetGenerateHtml() {
	pidStr := p.Ctx.URLParam("productID")
	pid, err := strconv.Atoi(pidStr)
	contentTmp, err := template.ParseFiles(filepath.Join(templatePath, "product.html"))
	if err != nil {
		p.Ctx.Application().Logger().Debug(err)
	}
	// 2. 获取html生成路径
	fileName := filepath.Join(htmlOutPath, "htmlProduct.html")
	// 3. 获取模板数据
	product, err := p.ProductService.GetProductById(int64(pid))
	if err != nil {
		p.Ctx.Application().Logger().Debug(err)
	}

	// 4. 生成静态文件
	gennerateStaticHtml(p.Ctx, contentTmp, fileName, product)
}

// 生成html静态文件
func gennerateStaticHtml(ctx iris.Context, template *template.Template, filename string, product *datamodels.Product) {
	if Exists(filename) {
		err := os.Remove(filename)
		if err != nil {
			ctx.Application().Logger().Error(err)
		}
	}
	file, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, os.ModePerm)
	if err != nil {
		ctx.Application().Logger().Error(err)
	}
	defer file.Close()
	err = template.Execute(file, product)
}

// 判断是否生成静态文件
func Exists(filename string) bool {
	_, err := os.Stat(filename)
	return err == nil || os.IsExist(err)

}

func (p *ProductController) GetDetail() *mvc.View {
	product, err := p.ProductService.GetProductById(3)
	if err != nil {
		p.Ctx.Application().Logger().Error(err)
	}
	return &mvc.View{
		Layout: "shared/productLayout.html",
		Name:   "product/view.html",
		Data: iris.Map{
			"product": product,
		},
	}
}

func (p *ProductController) GetOrder() mvc.View {
	pidStr := p.Ctx.URLParam("productID")
	userIDStr := p.Ctx.GetCookie("uid")
	pid, err := strconv.Atoi(pidStr)
	if err != nil {
		p.Ctx.Application().Logger().Debug("参数解析失败!")
	}
	product, err := p.ProductService.GetProductById(int64(pid))
	if err != nil {
		p.Ctx.Application().Logger().Debug(err.Error())
	}
	var orderId int64 = 0
	var showMessage string
	if product.ProductNum > 0 {
		// 扣除商品数量
		product.ProductNum -= 1
		err := p.ProductService.UpdateProduct(product)
		if err != nil {
			p.Ctx.Application().Logger().Debug(err.Error())
		}
		// 创建订单
		userId, err := strconv.Atoi(userIDStr)
		if err != nil {
			p.Ctx.Application().Logger().Debug(err.Error())
		}
		o := &datamodels.Order{
			UserId:      int64(userId),
			ProductId:   product.ID,
			OrderStatus: datamodels.OrderWait,
		}
		// 插入数据库
		orderId, err = p.OrderService.InsertOrder(o)
		if err != nil {
			p.Ctx.Application().Logger().Debug(err.Error())
		} else {
			showMessage = "抢购成功!"
		}
	}
	return mvc.View{
		Layout: "shared/productLayout.html",
		Name:   "product/result.html",
		Data: iris.Map{
			"orderId":     orderId,
			"showMessage": showMessage,
		},
	}
}
