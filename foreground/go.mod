module foreground

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/kataras/iris v0.0.2
	github.com/kataras/iris/v12 v12.1.8
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
)
