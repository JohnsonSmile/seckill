// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/3 14:01
// 文件名称 ：   auth.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package middleware

import (
	"foreground/encrypt"
	"github.com/kataras/iris/v12"
)

func AuthConProduct(ctx iris.Context) {
	uid := ctx.GetCookie("uid")
	sign := ctx.GetCookie("sign")
	code, err := encrypt.DePwdCode(sign)
	if err != nil {
		ctx.Application().Logger().Debug("必须先登录111")
		ctx.Redirect("/user/login")
		return
	}
	if uid == "" || uid != string(code) {
		ctx.Application().Logger().Debug("必须先登录")
		ctx.Redirect("/user/login")
		return
	}
	ctx.Next()
}
