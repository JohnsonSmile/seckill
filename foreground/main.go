// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/3 00:23
// 文件名称 ：   main.go
// 工程名称 ：   seckill
// 开发工具 ：   GoLand
//

package main

import (
	ctext "context"
	"foreground/common"
	"foreground/middleware"
	"foreground/repositories"
	"foreground/services"
	"foreground/web/controllers"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/context"
	"github.com/kataras/iris/v12/mvc"
)

func main() {

	// 1. 创建iris实例
	app := iris.New()

	// github.com/pkg/sftp
	// golang.org/x/crypto/ssh

	// 2. 设置log
	app.Logger().SetLevel("debug")

	// 3. 注册模板
	template := iris.HTML("./web/views", ".html").Layout("shared/layout.html").Reload(true)
	app.RegisterView(template)
	// 4. 设置静态目录
	app.HandleDir("/public", "./web/public")
	// 指定静态文件
	app.HandleDir("/html", "./web/htmlProductShow")
	// 5. 出现异常的跳转
	app.OnAnyErrorCode(func(ctx context.Context) {

		ctx.ViewData("message", "访问的页面出错!")
		ctx.ViewLayout("")
		ctx.View("shared/error.html")
	})
	// 连接数据库
	db, err := common.NewMysqlConn()
	if err != nil {
		app.Logger().Println(err)
	}

	ctx, cancel := ctext.WithCancel(ctext.Background())
	defer cancel()


	userManager := repositories.NewUserManager("user", db)
	userService := services.NewUserService(userManager)
	user := mvc.New(app.Party("/user"))
	user.Register(ctx, userService)
	user.Handle(new(controllers.UserController))

	productManager := repositories.NewProductManager("product", db)
	productService := services.NewProductService(productManager)
	orderManager := repositories.NewOrderManager("order", db)
	orderService := services.NewOrderService(orderManager)
	productParty := app.Party("/product")
	// 验证登录的中间件
	productParty.Use(middleware.AuthConProduct)
	product := mvc.New(productParty)
	product.Register(ctx, productService, orderService)
	product.Handle(new(controllers.ProductController))

	app.Run(
		iris.Addr(":8082"),
		iris.WithoutServerError(iris.ErrServerClosed),
		iris.WithOptimizations,
	)
}
